FROM python:3.8-buster AS import

# install all necessary deps
RUN apt-get update \
    && apt-get install -y -qq python-dev libxml2-dev libxslt1-dev antiword \
                              unrtf poppler-utils tesseract-ocr flac \
                              ffmpeg lame libmad0 libsox-fmt-mp3 sox libjpeg-dev swig \
                              python3-pip \
    && apt-get clean

# Install textract using pip3
RUN pip3 install --no-cache-dir textract scikit-learn pandas altair flask